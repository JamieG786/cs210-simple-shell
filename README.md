# CS210 Simple Shell

This was a group project created by Jamie Greenaway and Nicole Haggerty for the class CS210 at the University of Strathclyde.

This is a Simple Shell that was developed in C. This is run from the command line and provides all of the basic functionality found in a normal shell.

To run this program simple compile the ace4.c file and then run all from the command line.