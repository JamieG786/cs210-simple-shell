/**************************************************************************
 * Assessment Title: ACE4 Simple Shell
 *
 * Number of Submitted C Files: ace4.c, builtins.h, declerations.h
 *
 * Date: 
 *
 * 
 *
 *
 * Personal Statement: I confirm that this submission is all my own work.
 *
 *          (Signed) - Jamie Greenaway - 201713906
 *          (Signed) - Nicole Haggerty - 201708009
 *
 * 
 *
 **************************************************************************/

#ifndef BUILTINS
#define BUILTINS

/*This is the change directory function. It will check if there is any other arguments.
If there is not it will set the directory to home.
If there is it will then to set the new directory to the one the user specified.
If it does not exist it will display an error.*/
void cd(char *tokArray[MAX]) {
	
	if (tokArray[1] == NULL) {
		chdir(getenv("HOME"));	
	} else if(tokArray[2] != NULL) {
		printf("Too many arguments for cd function. Format - 'cd <directory>'.\n");
	} else { 
		if (chdir(tokArray[1]) !=0) {			
			perror(tokArray[1]);
		}
	}
}

/*This function will display the current path. It first checks if there are too many arguments.
If there is it will display an error message. If there isnt it will print the current path*/
void getPath(char *tokArray[MAX]) {
	if (tokArray[1] != NULL) {
		printf("Too many arguments for getPath function. Format - 'getPath'\n");
	} else {
		printf("Current path: %s \n", getenv("PATH"));
	}
}


/*This is the setpath function. It will first check the number of arguments.
It there is not enough or too many it will display an error message.
If not then it will set the new path to the one specified by the user*/
void setPath(char *tokArray[MAX]) {
	char temp_path[500];

	if (tokArray[1] == NULL) {
		printf("Not enough arguments for setPath function. Format - 'setPath <new path>'\n");
	} else if (tokArray[2] != NULL) {
		printf("Too many arguments for setPath function. Format - 'setPath <new path>'\n");
	} else {
		strcpy(temp_path, tokArray[1]);
		strcat(temp_path, ":");
		strcat(temp_path, getenv("PATH"));
		setenv("PATH", temp_path, 1);
		printf("New path: %s \n", getenv("PATH"));
	}

}

/*This is the alias function. 
The first thing it does is count the number of aliases in the system.
If the only thing entered by the user is 'alias' it will print all aliases in the system.
If the alias counter is 0 it will print a message saying there are no aliasesin the system.
If not then it will print all of the aliases in the system.
It will then check if there are enough aruguments to create an alias.
If there isnt it will print an error message.
If there is it will then count the number of tokens before running through a loop to get the whole command to be alised into the command variable.
It will then check if an alias under this name already exists.
If there is it will then inform the user that they have overwritten a previous alias.
If there isnt then it will check if there is space for another alias in the system.
Finally it will add the alias into the system (2d array).
*/
void alias(char *tokArray[MAX], char *aliases[ALIASMAX][2]) {
int aliasCounter = 0;
int tokCounter = 0;
char command[MAX] = "";

	//Number of alisas is counted
	for(int i = 0; i < ALIASMAX; i++) {
		if(aliases[i][0] != NULL) {
			aliasCounter++;
		}
	}

	if(tokArray[1] == NULL) {
		if(aliasCounter == 0) {
			printf("There are no aliases in the system\n");
			return;
		} else {
			for(int j = 0; j < aliasCounter; j++) {
				printf("%s - %s\n", aliases[j][0], aliases[j][1]);
				
			} return;
		}

	} else if(tokArray[2] == NULL) {
		printf("Not enough arguments to create an alias. Format - 'alias <name of alias> <command>'");
		return;
	}

	//Number of tokens is counted
	while (tokArray[tokCounter] != NULL) {
		tokCounter++;
	}

	for (int k = 2; k < tokCounter; k++) {
		strcat(command, tokArray[k]);
		strcat(command, " ");
	}

	for (int x = 0; x < aliasCounter ; x++) {
		if (strcmp(tokArray[1], aliases[x][0]) == 0) {
				printf("Overwriting existing alias...\n");
				aliases[x][1] =strdup(command);
				return;
		}
	}

	if (aliasCounter == ALIASMAX) {
		printf("There is already the max number of alises.\n");
		return;
	}

	aliases[aliasCounter][0] = strdup(tokArray[1]);
	aliases[aliasCounter][1] = strdup(command);

}

/*This is the remove alias function. First it will count the number of aliases in the system.
If there are no aliases in the system it will print a message to the user.
It will then check if there are enough or too many arguments to run the function.
If there is it will then display an error message to the user.
It will then run through a loop to find where the alias is that the user wants to remove.
If it cant find it the user will see an error message.
If the alias is found, it will remove it and then move all of the aliases below it in the array up one place.
*/
void unalias(char *tokArray[MAX], char *aliases[ALIASMAX][2]) {

	int aliasCounter = 0;
	int aliasFound = false;

	//Number of aliases is counted
	for(int j = 0; j < ALIASMAX; j++) {
		if(aliases[j][0] != NULL) {
			aliasCounter++;
		}
	}
	if (aliasCounter == 0) {
		printf("There are no alias to be removed\n");
		return;
	} else if (tokArray[1] == NULL) {
		printf("Not enough arguments for unalias functuon. Format - unalias <name of alias>\n");
		return;
	} else if (tokArray[2] != NULL) {
		printf("Too many argumentsfor unalias functuon. Format - unalias <name of alias>\n");
		return;
	}

	for (int i =0; i < (aliasCounter + 1); i++) {
		if(strcmp(tokArray[1], aliases[i][0]) == true) {
			int index = i;
			aliasFound = true;
			while(index < (aliasCounter)){	
				aliases[index][0] = aliases[index+1][0];		
				aliases[index][1] = aliases[index+1][1];
				index++;
			} 
			printf("Alias succesfully removed\n");			
			return;
		
		} else if(aliasFound == false) {
			printf("This alias does not exist\n");
		}
	}
}

#endif
