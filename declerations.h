/**************************************************************************
 * Assessment Title: ACE4 Simple Shell
 *
 * Number of Submitted C Files: ace4.c, builtins.h, declerations.h
 *
 * Date: 18/03/2019
 *
 * 
 *
 *
 * Personal Statement: I confirm that this submission is all my own work.
 *
 *          (Signed) - Jamie Greenaway - 201713906
 *          (Signed) - Nicole Haggerty - 201708009
 *
 * 
 *
 **************************************************************************/
#ifndef DECLERATIONS
#define DECLERATIONS

#define true 0
#define false 1
#define MAX 513
#define HISTORYMAX 20
#define CWDMAX 256
#define ALIASMAX 10
#define GREEN "\x1b[32m"
#define BLUE "\x1b[34m"
#define RED   "\x1B[31m"
#define RESET "\x1b[0m"

#endif