/**************************************************************************
 * Assessment Title: ACE4 Simple Shell
 *
 * Number of Submitted C Files: ace4.c, builtins.h, declerations.h
 *
 * Date: 
 *
 * 
 *
 *
 * Personal Statement: I confirm that this submission is all my own work.
 *
 *          (Signed) - Jamie Greenaway - 201713906
 *          (Signed) - Nicole Haggerty - 201708009
 *
 * 
 *
 **************************************************************************/
/*This is an implementation of a simple shell.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include "declerations.h" //This header has all of the constant variable definitions
#include "builtins.h" //This header has all of the built in functions

//Function declerations
void tokenizer(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]);
void parser(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]);
void forkProcesses(char *tokArray[MAX]);
void command_finder(char *tokArray[MAX], char *aliases[ALIASMAX][2], char input[MAX]);
void getPath(char *tokArray[MAX]);
void setPath(char *tokArray[MAX]);
void cd(char *tokArray[MAX]);
void convertToInt(char input[MAX], int len, char *tokArray[MAX],char *aliases[ALIASMAX][2]);
void readHistory(char *history[HISTORYMAX], char filename[], char *tokArray[MAX]);
void writeHistory(char *history[HISTORYMAX], char filename[]);
char** historyAdd( char input[MAX], char *history[19], char *tokArray[MAX]);
void historyPrint(char *history[19]);
void alias(char *tokArray[MAX], char *aliases[ALIASMAX][2]);
int invokeAlias(char input[MAX], char *aliases[ALIASMAX][2], char *tokArray[MAX]);
void unalias(char *tokArray[MAX], char *aliases[ALIASMAX][2]);
void loadAliases(char *aliases[ALIASMAX][2], char filename[512], char *tokArray[MAX]);
void saveAliases(char *aliases[ALIASMAX][2], char filename[512]);
void handleInput(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]);


int status = true;
char *history[HISTORYMAX];
int iterationCounter = 0;
int aliasCount = 0;

/*This is the main function of the program that will run when the program starts. It starts by setting the directory to the users directory.
It then loads in the aliases and history from file before printing the welcome message and calling the parser.
Once it has exited the parser it begind the termination sequence. It changes back to the original directory and then prints the termination message.*/
int main () {

char input[MAX];
char *tokArray[MAX];
char *homepath;
char filename[513] = "";
char *aliases[ALIASMAX][2] = {{0}};

	
	homepath = getenv("PATH");		
	chdir(getenv("HOME"));	

	fgets(filename,512,stdin);
	readHistory(history, ".hist_list.txt", tokArray);
	loadAliases(aliases, ".alias.txt", tokArray);
	printf("Simple Shell\n");
	printf("Group 10\n");

	parser(input, tokArray, aliases);

	saveAliases(aliases, ".alias.txt");
	writeHistory(history,".hist_list.txt");

	printf("Final path: %s \n", getenv("PATH"));
	setenv("PATH", homepath, 1);

	printf("\nTerminating.....\n");
	printf("Goodbye\n");
	return true;
}

/*This is the parser. It will take display the prompt, and then take in user input using the fgets function.
It will then run a validation check to make sure that the command is not over the maximum amount of characters for this given shell.
It will then check if the input is 'exit' and if it is it will exit the parser.
After that it will check if the input is an alias before sending the input into the handle input function*/
void parser(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]) {   //Loop for the whole console
char cwd[CWDMAX];

	//Loop for the whole shell
	do{
		printf(GREEN "%s:~" RESET, getlogin());
		printf(BLUE "%s" RESET,getcwd(cwd, sizeof(cwd)));
		printf(RED ">>>>😀️ " RESET);

		//Loop for user input
		if(fgets(input, 1024, stdin) != NULL){

			size_t length = strlen(input);
			int len = length - 1;

			historyAdd(input, history, tokArray);
			if (len > MAX){
				printf("Too many characters please try again.");
				printf("The max is 512 you had:%d\n", len);

			}else if (input[0] == '!'){
				convertToInt(input, len, tokArray, aliases);

			}else if (strtok(input, "\n")){
				if (input[0] != ' ') {
					if (strcmp(input, "exit") == true) {
						status = false;
					} else if (status == true) {
						aliasCount=0;
						if (invokeAlias(input, aliases, tokArray) == 3) {
							printf("Error: would cause infinite loop\n");
						}
						handleInput(input, tokArray, aliases);				
						}
					}
				}
		}else {
			status = false;
		}

	} while (status == true);

} 

/*This is a function which handles input. It calls the tokenizer before sending it to the command finder*/
void handleInput(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]) {
	if (input == NULL) {
	printf("f");
	} else {
	tokenizer(input, tokArray, aliases);
	command_finder(tokArray, aliases, input);
	}
}

/*This function will take the input and split it into seperate tokens. It will then store these tokens in an array*/
void tokenizer(char input[MAX], char *tokArray[MAX], char *aliases[ALIASMAX][2]) {
	
	char *token;
	int i = 0;
	
		token = strtok(input, "\n");

		token = strtok(input, " ");
		tokArray[0] = token;

		while(token != NULL) {
			i++;
			token = strtok(NULL, " ");
			tokArray[i] = token;
			
		}
}
/*This is the function that allows for default commands to run.*/
void forkProcesses(char *tokArray[MAX]) {
	pid_t pid, pid1;
	int forkStatus;

	pid = fork();

	if (pid == -1) { //error
		fprintf(stderr, "Fork Failed");
	} else if (pid == 0) { //child process
		if(execvp(tokArray[0], tokArray) == -1) {
			printf("Command not found - %s \n", tokArray[0]);
		}
		exit(0);
		pid1 = getpid();
		execvp(tokArray[0], tokArray);
	} else { //parent process
		pid1 = getpid();
		waitpid(pid, &forkStatus, 0);
		}
}


/*This is the command finder. It will run through the tokenized input and check if it is the same as any of our builtin commands. 
If it is it will then call the appropriate function for that builtin to run
If it is not it will send it into the fork function to check if it is a default command.*/
void command_finder(char *tokArray[MAX], char *aliases[ALIASMAX][2], char input[MAX]) {
	if (strcmp("cd", tokArray[0]) == true) {
		cd(tokArray);
	}
	else if (strcmp("getpath", tokArray[0]) == true) {
		getPath(tokArray);
	}	
	else if (strcmp("setpath", tokArray[0]) == true) {
		setPath(tokArray);
	}
	else if (strcmp("history", tokArray[0]) == true) {
		historyPrint(history);
	}
	else if (strcmp("alias", tokArray[0]) == true) {
		alias(tokArray, aliases);
		saveAliases(aliases,"alias.txt");
	}
	else if (strcmp("unalias", tokArray[0]) == true) {
		unalias(tokArray, aliases);
	} else if(strcmp("exit", tokArray[0]) == 0) {
		status = false;
	}else if(invokeAlias(input, aliases, tokArray) == true) { 
			handleInput(input, tokArray, aliases);
	}else if(invokeAlias(input, aliases, tokArray) == 3) { 
		printf("Error: would cause infinite loop\n");
	}else {
		forkProcesses(tokArray);
	}
}


/*This is the function that will load the aliases in from file. 
It reads them in line by line and then calls the alias method for them to be added into the 2d array*/
void loadAliases(char *aliases[ALIASMAX][2], char filename[], char *tokArray[MAX]) {
	char ch;
	int i = 0;
	int tokCounter = 0;
	char command[MAX] = "";
	FILE *file;

	if (file == NULL) {
		perror("Alias: ");
		return;
   	}
	file = fopen(filename, "r");
	while(fgets(command,sizeof command, file) !=NULL) {
		tokenizer(command, tokArray, aliases);
		alias(tokArray, aliases);

	}		

	fclose(file);
	
}

/*This is the function that saves the aliases to file. it runs through the 2d array and stores all of the alises to a txt file.*/
void saveAliases(char *aliases[ALIASMAX][2], char filename[]) {

	FILE *file;

	file = fopen(filename, "w");

	if (file == NULL) {
		perror("Alias: ");
		return;
   	}
	int i = 0;
	while(aliases[i][0] != NULL) {
	fprintf(file, "alias %s %s\n", aliases[i][0], aliases[i][1]); 
	i++;
	}

	fclose(file);
}

/*This is the function that checks if the input is the same as an alias.
If it is it will then copy the alias command into input so that it can be sent for tokenising*/
int invokeAlias(char input[MAX], char *aliases[ALIASMAX][2], char *tokArray[MAX]) {
	aliasCount++;
	
	if (aliasCount > 30) {
		return 3;
	}
	int j = 1;
	char originalInput[MAX];
	char *tempTokArray[MAX];
		strcpy(originalInput, input);
		tokenizer(originalInput, tokArray, aliases);
		for (int i = 0; i < ALIASMAX; i++) {
			if (aliases[i][0] == NULL) {
				return false;
			} else {
				if (strcmp(input, aliases[i][0]) == true) {
						strcpy(originalInput, aliases[i][1]);
						while (tokArray[j] != NULL) {
							printf("%s", tokArray[j]);
							strcat(" ", originalInput);
							strcat(tokArray[j], originalInput);
							j++;
						}
						strcpy(input, originalInput);
						return true;
					}
				} 
		}
}

/*This is a method invoked when the user inputs anything starting with a '!'.
It converts the necessary chars into an array and catches any errors.
After calculating the correct history counter to call it passes the information
into the handleInput method to be tokinized and to call the correct command.*/
void convertToInt(char input[MAX], int len, char *tokArray[MAX], char *aliases[ALIASMAX][2] ) {
int integer = 0;
	char c1 = input[1];
	char c2 = input[2];
	char c3 = input[3];
	int integer1 = c1 - '0';
	int integer2 = c2 - '0';
	int integer3 = c3 - '0';
	int counter = 0;

	if (len == 2){
		integer = integer1;
	} else if (len == 3) {
		integer = (integer1 * 10) + integer2;
	}
	if (c1 == '!'){
		input = strdup(history[iterationCounter-1]);
		 handleInput(input, tokArray, aliases);

	} else if(c1 == '-'){

		if (len == 3){
			integer = integer2;
		} else if(len == 4) {
			integer = (integer2 * 10) + integer3;
		}

		if(integer < 1 || integer > 20 ){
			printf("Only 20 commands are stored please enter a number between 1 and 20.\n");
		}else{
			counter = (iterationCounter % HISTORYMAX) + integer -1;

			if (counter > 19 ){
				counter = counter - 20;
			}
			
			if (history[counter] == NULL) {
				printf("No command stored at this point in history\n");
			} else {
				input = strdup(history[counter]);				
				handleInput(input, tokArray, aliases); 
			}
		}
	  
	}else{
		if(integer < 1 || integer > 20 ){
			printf("Only 20 commnds are stored please enter a number between 1 and 20.\n");
		}else{
			if (integer > iterationCounter){
				counter = (iterationCounter % HISTORYMAX) + 20 - integer;
			}else{
			counter = (iterationCounter % HISTORYMAX) - (integer);
			}

			if (history[counter] == NULL) {
				printf("No command stored at this point in history\n");
			} else {
				input = strdup(history[counter]);				
				handleInput(input, tokArray, aliases); 
			}
	}}
}




/*This is the function that reads the history file , the file name is passed in from the main method.
If a file is found it is opened, the information is written into the history array and then the file is closed. 
If a file is not found an error message is displayed.*/
void readHistory(char *history[HISTORYMAX], char filename[], char *tokArray[MAX]) {
	FILE *file;
    file = fopen(filename, "r");
  
    if (file == NULL) {
	perror("History: ");
	return;
    }

    char line[MAX];

    while(fgets(line, sizeof line, file)!=NULL) {
    	historyAdd(line, history, tokArray);
    }


    fclose(file);

}

/*This is the function that writes to the file, the file name is passed in from the main method.
If a file is found it is opened edited then closed. If a file is not found an error message is displayed*/
void writeHistory(char *history[HISTORYMAX], char filename[]) {
	FILE *file = fopen(filename, "w");

    file = fopen(filename, "w");
  	if (file == NULL) {
    	fprintf(stderr, "Can't open %s\n", filename);
    	exit(1);
  	}

  	int i = iterationCounter--;
  	
  	for (int n = 0; n < 20; n++) {
    	fputs(history[i % HISTORYMAX], file);
    	i++;
  	}
  	fclose(file);

}

/*This is the function adds values to the history array.
it then intalizes the iteration counter*/
char** historyAdd( char input[MAX], char *history[HISTORYMAX], char *tokArray[MAX]) {
	if (input[0] != '!' && input[0] != '\n'){
		
		history[iterationCounter] = strdup(input);
		iterationCounter++;
	}

	if(iterationCounter==20) iterationCounter=0;
    return history;
}

/*This is the function that prints the contents of history.*/
void historyPrint(char *history[HISTORYMAX]) {
    int i = iterationCounter;
    int index = 20;

	printf("the history is \n");

   for(int n = 20; n > 0; n--){
		printf("%d ) %s \n",index, history[i++ % HISTORYMAX]);

	index--;
    }
}
